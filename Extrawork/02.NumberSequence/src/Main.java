import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>(); //вводим динамический массив,т.к. не знаем сколько элементов будет введено
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите элементы массива:");
        int value;
        while ((value = scanner.nextInt()) != -1) {     //формируем массив, с которым будем работать (пока не ввели -1)
            if (value < -100 || value > 100) {          // проверяем, что элеенты в диапазоне [-100;100]
                System.out.println("Некорректное число, введите заново");
            } else {
                arrayList.add(value);
            }
        }

        System.out.println("Ввод окончен");
        System.out.println(arrayList);
        solveProblem(arrayList);
    }

    public static int[] repeatCountArray(ArrayList<Integer> array) {   // Метод подсчитывает сколько каждый элемент встречается в массиве и возвращает
        int[] additionalArray = new int[array.size()];               // массив из количества повторений для каждого элемента исходного массива
        for (int i = 0; i < array.size(); i++) {
            for (int j = 0; j < array.size(); j++) {
                if (array.get(j) == array.get(i)) {
                    additionalArray[i] += 1;
                }

            }
        }
        return additionalArray;
    }

    public static ArrayList<Integer> searchMinimal(ArrayList<Integer> array) {   // метод находит индексы элементов исходного массива, которые
        ArrayList<Integer> finalIndexArray = new ArrayList<>();                  // встречаются наименьшее число раз. Возвращает массив из этих
        int[] transitArray = repeatCountArray(array);                                // индексов. Массив динамический, т.к. мы не знаем его размер
        int minimal = transitArray[0];
        for (int j : transitArray) {
            if (j < minimal) {
                minimal = j;
            }
        }

        for (int i = 0; i < array.size(); i++) {
            if (transitArray[i] == minimal) {
                finalIndexArray.add(i);
            }
        }
        return finalIndexArray;

    }
    public static int searchMinimalCount(ArrayList<Integer> array) {  // Метод находит наименьшее число повторений
        int[] repeatCountArray = repeatCountArray(array);
        int minimal = repeatCountArray[0];
        for (int j : repeatCountArray) {
            if (j < minimal) {
                minimal = j;
            }
        }
        return minimal;
    }
    public static void solveProblem(ArrayList<Integer> array) {         // Метод выводит элементы, встречающиеся наименьшее число раз,
        ArrayList<Integer> finalPrintArray = new ArrayList<>();         //Выполняя проверку, что нет дублей (один и тот же элемент не выводится несколько раз)
        ArrayList<Integer> minimal = searchMinimal(array);
        int minCount = searchMinimalCount(array);
        for (Integer integer : minimal) {
            if (!finalPrintArray.contains(array.get(integer))) {
                finalPrintArray.add(array.get(integer));
            }
        }
        for (Integer integer : finalPrintArray) {
            System.out.println(integer + " встречается" + " " + minCount + " раз");
        }

    }

}





