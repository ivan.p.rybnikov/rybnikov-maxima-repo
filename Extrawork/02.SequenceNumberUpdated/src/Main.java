import java.util.Scanner;

public class Main {
    static int[] arrayIndexes = new int[200];
    public static void main(String[] args) {
         //максимальное число различных элементов массива
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите элементы массива:");
        int value;
        while ((value = scanner.nextInt()) != -1) {     // формируем массив, с которым будем работать (пока не ввели -1)
            if (value < -100 || value > 100) {          // проверяем, что элементы в диапазоне [-100;100]
                System.out.println("Некорректное число, введите заново");
            } else {
                int idx = getIndex(value);
                arrayIndexes[idx]++;
            }
        }
        solve();
    }
    public static int searchMinimal(int[] array) {
        int min = -1;
        for (int i : array) {
            if (i == 0) {
                continue;
            }
            if (i < min || min == -1) {
                min = i;
            }
        }
        return min;
    }

    private static int getIndex(int value) {
        int index = value + 100;
        if (value > -1) {
            index--;
        }
        return index;
    }

    private static int getValue(int index) {
        int value = index - 100;
        if (index >= 99) {
            value ++;
        }
        return value;
    }

    public static  void solve() {
        int minCount = searchMinimal(arrayIndexes);
        for (int i = 0; i < arrayIndexes.length; i++) {
            if (arrayIndexes[i] == minCount) {
                int value = getValue(i);
                System.out.println(value + " встречается " + minCount + " раз");
            }
        }
    }
}
