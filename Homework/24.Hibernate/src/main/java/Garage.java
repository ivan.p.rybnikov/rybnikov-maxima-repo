//import org.hibernate.annotations.Table;
//import org.hibernate.mapping.List;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "garage")
public class Garage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToMany(mappedBy = "id")
    private List<Car> cars;
    @Column(name = "number")
    int number;

    public Garage() {
    }
    public Garage(List<Car> cars) {
        this.cars = cars;
    }


    public List<Car> getCars() {
        return cars;
    }

    public int getNumber() {
        return number;
    }


    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Garages{" +
                "number='" + number + '\'' +
                ", cars='" + cars.toString() +
                '}';
    }
}
