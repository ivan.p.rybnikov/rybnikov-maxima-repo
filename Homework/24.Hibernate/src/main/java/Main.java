import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


import java.util.List;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();

        configuration.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/postgres");
        configuration.setProperty("hibernate.connection.username", "postgres");
        configuration.setProperty("hibernate.connection.password", "379159");
        configuration.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL95Dialect");
        configuration.setProperty("hibernate.show_sql", "true");
        configuration.addAnnotatedClass(Car.class);
        configuration.addAnnotatedClass(Garage.class);

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

        // Выводим гаражи, где есть машина Пежо
        // language=HQL

        //noinspection unchecked
        List<Integer> garageNumbers = session.createQuery("select g.number from Garage as g left join g.cars as c where c.model = 'Pegeaut' group by number").getResultList();
        System.out.println(garageNumbers);

        // Выводим гаражи, где есть машина у Путина
        //noinspection unchecked
        List<Integer> garageNumbersPutin = session.createQuery("select g.number from Garage as g left join g.cars as c left join c.owner as o where o.lastName = 'Putin' group by number").getResultList();
        System.out.println(garageNumbersPutin);
    }
}
