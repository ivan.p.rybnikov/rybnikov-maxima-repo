package com.example.freemarker.Controller;
import com.example.freemarker.domain.Message;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class FreeMarkerController {

    @GetMapping("/form")
    public String form(
//            @RequestParam ("name") String name,
            Model model
    ) {
        String name = "Antony";
        model.addAttribute("name",name);
        Message message1 = new Message("Hi, nice to see you!","Ivan");
        Message message2 = new Message("Hi, its a good idea!!","Tom");
        Message message3 = new Message("Hi, see you tomorrow!","Andrew");

        List<Message> messages = new ArrayList<>();
        messages.add(message1);
        messages.add(message2);
        messages.add(message3);

        //HashMap<Integer,Message> messages = new HashMap<>();
        //messages.put(message1.getId(),message1);
        //messages.put(message2.getId(),message2);
        //messages.put(message3.getId(),message3);

        model.addAttribute("messages",messages);


        return "form" ;

    }

}

