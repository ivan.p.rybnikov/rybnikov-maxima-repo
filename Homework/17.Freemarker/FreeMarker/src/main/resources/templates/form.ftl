<#-- @ftlvariable name="message" type="java.lang.String" -->

<!DOCTYPE html>
<html>
<body>
<p>Hello ${name}! You have the following messages:</p>
<#list messages as m>
    <p><b>${m.author}:</b> ${m.text}</p>
</#list>
</body>
</html>