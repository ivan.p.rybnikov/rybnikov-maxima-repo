import java.util.Random;

public class TurnImpl implements Console{
    static int startIndex;

    public static int getStartIndex() {
        return startIndex;
    }

    @Override
    public void turn(String value, TV tv) {
        Random generator = new Random();
        int randomIndexChannel = generator.nextInt(tv.channels.length);
        int randomIndexProgramm = generator.nextInt(tv.channels[randomIndexChannel].programs.length);
        startIndex = randomIndexChannel;

        System.out.println(tv.channels[randomIndexChannel].name);
        System.out.println(tv.channels[randomIndexChannel].programs[randomIndexProgramm].getName());
    }
}
