import java.text.ParseException;
import java.util.Scanner;

public class Main {

    public Main()  {
    }

    public static void turn (Console console,TV tv, String value){
        console.turn(value,tv);
    }
    public static void main(String[] args) throws ParseException {
        TV tv = new TV();
        String value;

        TurnImpl turn = new TurnImpl();
        DigitTurnImpl digitTurn = new DigitTurnImpl();
        TurnNextPreviousChannel turnNextPreviousChannel = new TurnNextPreviousChannel();
        Scanner scanner = new Scanner(System.in);


        do {
            System.out.println("Включите телевизор");
            value = scanner.next();
        } while (!value.equals("turn"));

        turn(turn, tv, value);
        tv.currentIndex = TurnImpl.getStartIndex();

        value = scanner.next();

        do {

            switch (value) {

                case ">":
                    turn(turnNextPreviousChannel, tv, String.valueOf(tv.currentIndex + 1));
                    tv.lastIndex = tv.currentIndex;
                    tv.currentIndex += 1;
                    break;
                case "<":
                    turn(turnNextPreviousChannel, tv, String.valueOf(tv.currentIndex - 1));
                    tv.lastIndex = tv.currentIndex;
                    tv.currentIndex -= 1;
                    break;

                case "last":
                    turn(turnNextPreviousChannel, tv, String.valueOf(tv.lastIndex));
                    tv.lastIndex = tv.currentIndex;
                    tv.currentIndex -= 1;
                    break;


                default:
                    if (value.matches("[0-9]+")) {
                        turn(digitTurn, tv, value);
                        tv.lastIndex = tv.currentIndex;
                        tv.currentIndex = Integer.parseInt(value);
                        break;

                    }
            }

            value = scanner.next();

        }while (!value.equals("stop"));


    }
}
