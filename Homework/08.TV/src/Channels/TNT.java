package Channels;

import Programms.*;

public class TNT extends Channel{
    public TNT ()  {
        this.name = "ТНТ";
        programs = new Program[3];
        programs[0] = new ComedyClub();
        programs[1] = new Extrasenses();
        programs[2] = new Interns();

    }

}
