package Channels;

import Programms.*;


public class Russia extends Channel {
    public Russia() {
        programs = new Program[3];
        this.name = "Россия";
        programs[0] = new Vesti();
        programs[1] = new GoodNightBabies();
        programs[2] = new WeekNews();

    }
}
