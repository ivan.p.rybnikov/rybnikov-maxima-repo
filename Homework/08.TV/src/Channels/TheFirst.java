package Channels;

import Programms.*;

public class TheFirst extends Channel{
    public TheFirst()  {
        this.name = "Первый";
        programs = new Program[5];
        programs[0] = new News();
        programs[1] = new LetSpeak();
        programs[2] = new LetsGoMarried();
        programs[3] = new Pozner();
        programs[4] = new WhatWhereWhen();
    }
}
