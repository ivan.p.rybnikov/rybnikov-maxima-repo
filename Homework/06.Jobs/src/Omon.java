public class Omon extends Policeman{
    boolean warReady;
    public Omon(String name, String lastName, int age, String grade, String office,boolean warReady) {
        super(name, lastName, age, grade, office);
        this.warReady = warReady;
    }
    @Override
    public void job(){
        System.out.println("Hello! My job is Omon. My war ready is " + warReady );
    }
}
