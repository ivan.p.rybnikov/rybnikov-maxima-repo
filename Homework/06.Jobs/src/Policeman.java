public class Policeman extends Human {
    String grade;
    String office;


    public Policeman(String name, String lastName, int age, String grade, String office) {
        super(name, lastName, age);
        this.grade = grade;
        this.office = office;

    }

    @Override
    public void sleep (){
        System.out.println("Hello! My name is " + getName() + " I don't sleep" );
    }
    @Override
    public void eat(){
        System.out.println("Hello! My age is " + getAge() + " I don't eat" );
    }
    @Override
    public void job(){
        System.out.println("Hello! My job is policeman. My grade is " + grade );
    }
}
