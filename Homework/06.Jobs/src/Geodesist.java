public class Geodesist extends Human{
    String qualification;
    int experience;


    public Geodesist(String name, String lastName, int age, String qualification, int experience) {
        super(name, lastName, age);
        this.qualification = qualification;
        this.experience = experience;
    }

    @Override
    public void sleep (){
       System.out.println("Hello! My name is "+ getName() + " I like to sleep ");
    }
    @Override
    public void eat(){
        System.out.println("Hello! My age is " + getAge() +  " I like to eat" );
    }
    @Override
    public void job(){
        System.out.println("Hello! My job is geodesic. My qualification is "+ qualification );
    }
}
