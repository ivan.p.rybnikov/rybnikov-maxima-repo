public class Teacher extends Human{
    String school;
    String level;


    public Teacher(String name, String lastName, int age, String school, String level) {

        super(name, lastName, age);
        this.school = school;
        this.level = level;
    }

    @Override
    public void sleep (){
        System.out.println("Hello! My name is " + getName() + " I like to sleep" );
    }
    @Override
    public void eat(){
        System.out.println("Hello! My age is " + getAge() + " I have regular dinner" );
    }
    @Override
    public void job(){
        System.out.println("Hello! My job is teacher. My level is " + level );
    }
}
