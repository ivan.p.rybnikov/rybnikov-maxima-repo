public class MainPoliceman {
    public static void main(String[] args) {
        Policeman policeman = new Policeman("Anton","Romankov",35,"Leutenant","Columbia");
        policeman.eat();
        policeman.say();
        policeman.sleep();
        policeman.job();
    }
}
