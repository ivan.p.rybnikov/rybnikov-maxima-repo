import java.util.Arrays;

public class InverseArray {
    public static void main(String[] args) {
        int[] array = {1,2,4,6,35,46,57,57,0,4,3,6};
        int[] reverseArray = inverseArray(array);

        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(reverseArray));
    }
    private static int[] inverseArray(int[] array){
        int[] reverseArray = new int[array.length] ;
        for (int i=0; i<array.length;i++){
            reverseArray[i] = array[(array.length-i)-1];
            }
        return reverseArray;
        }
}
