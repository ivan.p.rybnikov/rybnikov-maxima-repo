import java.util.Arrays;

public class ChangeToZero {

        public static void main(String[] args) {
            int[] array = {1,2,4,6,35,46,57,57,0,4,3,6};
            int number = 6;
            if (ChangeToZero(array,number)>0){
            System.out.println(Arrays.toString(array));}
            else System.out.println("Данное число отсутствует в массиве");
        }
        private static int ChangeToZero(int[] array,int number){
            int counter =0;
            for (int i=0; i<array.length;i++){

                if(array[i]==number){
                    array[i] = 0;
                    counter++;
                }
            }
            return counter;
        }
}




