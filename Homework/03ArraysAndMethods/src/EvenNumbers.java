import java.util.Arrays;

public class EvenNumbers {
    static int[] array = {2, 1, 1, 1, 1, 1, 1, 2, 1, 1,4,8,5,6,4,3,8,9};

    public static void main(String[] args) {
        int[] targetArray = evenArray(array);

        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(targetArray));

    }

    static int[] evenArray(int[]array){
        int[] targetArray = new int[countEvens(array)];
        int indexEvenArray = 0;
        for (int i : array) {
            if (i % 2 == 0) {
                targetArray[indexEvenArray++] = i;
                //i2++;
            }
        }
        return targetArray;
    }
        private static int countEvens ( int[] array){
            int evens = 0;
            for (int i = 0; i < array.length; i++) {
                if ((array[i] % 2) == 0) {
                    evens += 1;
                }
            }
            return evens;
        }

    }

