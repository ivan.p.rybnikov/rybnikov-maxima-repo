import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {1, 2, 4, 6, 99, 46, 57, 57, 0, 4, 3, 6};
        int[] anotherArray = bubbleSortReturned(array);
        bubbleSortReturned(anotherArray);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(anotherArray));
    }

    public static void bubbleSortVoid(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    public static int[] bubbleSortReturned(int[] array) {
        int[] resultArray = new int[array.length];
        for(int i=0;i<array.length;i++){
            resultArray[i]=array[i];
        }
        bubbleSortVoid(resultArray);
        return resultArray;
    }

}