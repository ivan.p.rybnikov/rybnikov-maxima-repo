package ru.maxima.dao;

import ru.maxima.model.User;

import java.util.List;

public interface UserDao {
    List<User> getAll();


    List<User> getUserByEmail(String email);

    void add(User user);

}
