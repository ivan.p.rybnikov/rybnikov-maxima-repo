package ru.maxima.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.maxima.dao.UserDao;
import ru.maxima.model.User;

import java.util.List;

@Component  // создаём бин hibernateUserDao
public class HibernateUserDao implements UserDao {

    // внедряем бин sessionFactory который создали в persistence-config
    @Autowired
    private SessionFactory sessionFactory;

    // создаём сессию из sessionFactory в отдельном методе
    private Session currentSession() {
        return sessionFactory.openSession();
    }

    @Override
    public List<User> getAll() {
        Session session = currentSession(); // получаем сессию
        // language=HQL
        List<User> users = session.createQuery("from User",User.class).list(); // в полученной сессии отправляем HQL
        // где указываем из какой сущности достать данные и как их вернуть (в списке)
        return users;
    }

    @Override
    public void add(User user) {
        Session session = currentSession(); // вызываем сессию
        session.save(user); // вызываем метод save на сессии и сохраняем сущность в таблицу
    }

    @Override
    public List<User> getUserByEmail(String email) {
        return null;
    }
}
