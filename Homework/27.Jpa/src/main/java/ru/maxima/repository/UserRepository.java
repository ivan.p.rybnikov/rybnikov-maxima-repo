package ru.maxima.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.model.User;

import java.util.List;

@Repository // разновидность аннотации @Component для репозиториев, создаём бин userRepository
public interface UserRepository extends JpaRepository<User, Long> { // Создаём именно интерфейс, реализацию не пишем
    User findByEmail(String email);                                 // query method задаем метод через название

    List<User> findByFirstnameStartingWith(String prefix);
    List<User> findByEmailEndingWith(String domain);
}                                                                   // Отличие JpaRepository от CrudRepository

