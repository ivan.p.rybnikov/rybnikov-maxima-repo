package ru.maxima.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import ru.maxima.dao.UserDao;
import ru.maxima.model.User;


import java.util.List;

@Controller
public class UserEmailController {
    @Autowired
    @Qualifier("jpaUserDao")
    private UserDao userDao;

    @GetMapping("/users/email")
    public String getUsersByEmail(Model model,@RequestParam String email){
        List<User> users = userDao.getUserByEmail(email);
        model.addAttribute("usersAttr", users);
        return "/users";
    }
}
