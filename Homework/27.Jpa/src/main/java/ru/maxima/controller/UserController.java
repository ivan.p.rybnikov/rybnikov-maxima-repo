package ru.maxima.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.maxima.dao.UserDao;
import ru.maxima.model.User;
import ru.maxima.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Controller // создаём бин
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/users")
    public String getUsers(Model model){
        List<User> users = service.getAll();
        model.addAttribute("usersAttr", users);
        return "/users";
    }

    @GetMapping("/users/new")
    public String getSignUp(){
        return "/sign_up";
    }

    @PostMapping("/users/new")
    public String signUp(@ModelAttribute User user){

        service.add(user);
        return "redirect:/users";

    }

    @GetMapping("/users/firstname")
    public String getUserByFirstnamePrefix(Model model, @RequestParam String prefix){
        List<User> users = service.findByFirstnameStartingWith(prefix);
        model.addAttribute("usersAttr", users);
        return "/users";
    }
    @GetMapping("/users/emaildomain")

    public String getUserByEmailDomain(Model model, @RequestParam List<String> domains){
        List<User> users = new ArrayList<>();
        for (String domain : domains) {
           List<User> domainEmailUsers = service.findByEmailEndingWith(domain);
           users.addAll(domainEmailUsers);
        }
        model.addAttribute("usersAttr", users);
        return "/users";
    }
}



