package ru.maxima.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.model.User;
import ru.maxima.repository.UserRepository;

import java.util.List;

@Service    // создаём бин userService
@Transactional(readOnly = true) // помечаем методы класса только на чтение
public class UserServiceImpl implements UserService {

    @Autowired  // внедряем бин userRepository
    private UserRepository userRepository;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();    // используем специальный метод findAll() из userRepository
    }

    @Override
    @Transactional // здесь аннотация помечаем метод на запись
    public void add(User user) {
        userRepository.save(user);  // метод save() добавляет пользователя, если его не было и изменяет, если уже был
    }

    @Override
    public List<User> findByFirstnameStartingWith(String prefix) {
        return userRepository.findByFirstnameStartingWith(prefix);
    }

    @Override
    public List<User> findByEmailEndingWith(String domain) {
        return userRepository.findByEmailEndingWith(domain);
    }


}
