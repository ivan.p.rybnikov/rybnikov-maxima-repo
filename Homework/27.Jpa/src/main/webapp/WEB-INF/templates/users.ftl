<html>
<head>
    <title>Users</title>
</head>
<body>
<#-- во View users мы ищем аттрибут usersAttr и подставляем вместо аттрибута его значение список пользователей-->
<#if usersAttr?has_content>
    <ul>
        <#list usersAttr as user>
            <li>${user.firstname} ${user.lastname} ${user.email}</li>
        </#list>
    </ul>
<#-- если нет пользователей в списоке -->
    <#else >
    <p>No users</p>
</#if>
</body>
</html>