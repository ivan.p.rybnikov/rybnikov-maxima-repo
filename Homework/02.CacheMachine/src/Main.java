import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Вводим переменные, соответствующие купюрам и их количеству в запрашиваемой сумме
        int piatak = 5000;
        int piatakCount = 0;
        int dvushka = 2000;
        int dvushkaCount = 0;
        int kosar = 1000;
        int kosarCount = 0;
        int piatihat = 500;
        int piatihatCount = 0;
        int dvesotki = 200;
        int dvesotkiCount = 0;
        int sotka = 100;
        int sotkaCount = 0;
        int poltos = 50;
        int poltosCount = 0;
        double accountSum = 100000.22; //Сумма на счету
        int sum = 0;

        System.out.print("Введите сумму: "); //просим ввести сумму
         sum = scanner.nextInt();// вводим сумму

        while(!checks(accountSum, sum)){
            notifications(accountSum,sum);
            System.out.print("Введите сумму: "); //просим ввести сумму
            sum = scanner.nextInt();// вводим сумму

        };



        System.out.print("Выдать с разменом?:");   // Предлагаем выдать сумму с разменом (значения true, false)
        boolean choice = scanner.nextBoolean();
        if (!choice) {
              piatakCount = countMoney(sum,piatak);
              sum = sum - piatakCount * piatak;

              dvushkaCount = countMoney(sum,dvushka);
              sum = sum - dvushkaCount*dvushka;

              kosarCount = countMoney(sum,kosar);
              sum = sum - kosarCount*kosar;

              piatihatCount = countMoney(sum,piatihat);
              sum = sum - piatihatCount*piatihat;

              dvesotkiCount = countMoney(sum,dvesotki);
              sum = sum - dvesotkiCount*dvesotki;

              sotkaCount = countMoney(sum,sotka);
              sum = sum - sotkaCount*sotka;

              poltosCount = countMoney(sum,poltos);

        } else {
            if (countMoney(sum,piatak) > 0) {              // Определяем кол-во купюр для размена
                dvushkaCount = countMoney(sum,dvushka);
                sum = sum - dvushkaCount * dvushka;
            }
            if (countMoney(sum,dvushka) > 0) {
                kosarCount = countMoney(sum,kosar);
                sum = sum - kosarCount * kosar;
            }
            if (countMoney(sum,kosar) > 0) {
                piatihatCount = countMoney(sum,piatihat);
                sum = sum - piatihatCount * piatihat;
            }
            if (countMoney(sum,piatihat) > 0) {
                sotkaCount = countMoney(sum,sotka);
                sum = sum - sotkaCount * sotka;
            }
            if (countMoney(sum,dvesotki) > 0) {
                dvesotkiCount = countMoney(sum,dvesotki);
                sum = sum - dvesotkiCount * dvesotki;
            }
            if (countMoney(sum,poltos)>0) {
                poltosCount = countMoney(sum,poltos);
            }
        }

        if (piatakCount > 0) {                        // Выводим кол-во купюр в консоль
            System.out.println("5000 - " + piatakCount + "шт.");
        }
        if (dvushkaCount > 0) {
            System.out.println("2000 - " + dvushkaCount + "шт.");
        }
        if (kosarCount > 0) {
            System.out.println("1000 - " + kosarCount + "шт.");
        }
        if (piatihatCount > 0) {
            System.out.println("500 - " + piatihatCount + "шт.");
        }
        if (dvesotkiCount > 0) {
            System.out.println("200 - " + dvesotkiCount + "шт.");
        }
        if (sotkaCount > 0) {
            System.out.println("100 - " + sotkaCount + "шт.");
        }
        if (poltosCount > 0) {
            System.out.println("50 - " + poltosCount + "шт.");
        }

    }

    private static void notifications(double accountSum, int sum) {
        boolean check = ((accountSum - sum) > 0) && !((sum % 50) > 0) ;

        if ((accountSum - sum) < 0 ) {         //  Проверяем что остаток не превышен и сумма может быть выдана (кратна 50)
            System.out.println("Превышен остаток по счёту ");
        }

        else if ((sum % 50) > 0) {
            System.out.println("Невозможно выдать сумму ");
        }
    }
    private static boolean checks(double accountSum, int sum){
        boolean check = ((accountSum - sum) >= 0) && !((sum % 50) > 0) ;
        return check;
    }

    public static int countMoney(int sum, int banknot) {
        int banknotCount = 0;
        if (sum - banknot >= 0) {               // Определяем кол-во купюр указанного номинала в указанной сумме
            banknotCount = sum / banknot;


        }
        return banknotCount;
    }
}
