import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {
    static ArrayList<Car> cars = new ArrayList<>();

    static File file = new File("CarsCatalog.txt");

    public static void main(String[] args) {
        createFile(file);
        fillList();

        Scanner scanner = new Scanner(System.in);

        boolean addCar;
        boolean addFilter;

        do {
            System.out.println("Хотите добавить машину?(true/false)");
            addCar = scanner.nextBoolean();
            if (addCar) {
                addCar();

            }
        }
        while (addCar);


        System.out.println("Вывести машины на экран?(true/false)");
        boolean choice = scanner.nextBoolean();
                showResults();

        do {
            System.out.println("Хотите отфильтровать?(true/false)");
            addFilter = scanner.nextBoolean();
            if (addFilter) {
                System.out.println("Введите:" + " 1-Фильтр по пробегу" + "," + "2-Фильтр по наличию" + " 3-Фильтр по состоянию");
                int filterCase = scanner.nextInt();
                switch (filterCase) {
                    case (1):
                        System.out.println("Введите максимальный пробег:");
                        int mileage = scanner.nextInt();
                        filterMileage(mileage);
                        break;

                    case (2):
                        System.out.println("Машина в наличии?(true/false)");
                        boolean exist = scanner.nextBoolean();
                        filterExist(exist);
                        break;

                    case (3):
                        System.out.println("Машина битая?(true/false)");
                        boolean damaged = scanner.nextBoolean();
                        filterCondition(damaged);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + filterCase);
                }

            } else {
                break;
            }
        } while (addFilter);

    }


    public static void addCar() {
        Scanner scanner = new Scanner(System.in);


        Writer writer = null;
        System.out.println("Введите машину: model|brand|color|enginePower|mileage|exist|damaged");

        try {
            int carId = getLineCountByReader(file) + 1;

            writer = new FileWriter(file, true);
            String l = scanner.nextLine();
            writer.write(carId + "|" + l + "\n");
            addCarToList(l);
        } catch (NullPointerException | IOException e) {
            System.out.println("Не удалось записать");
        } finally {
            try {
                writer.close();
            } catch (NullPointerException | IOException e) {
                System.out.println("Не удалось записать");
            }

        }


    }

    public static int getLineCountByReader(File fileName) throws IOException {
        try (var lnr = new LineNumberReader(new BufferedReader(new FileReader(fileName)))) {
            while (lnr.readLine() != null) ;
            return lnr.getLineNumber();
        }
    }


    public static void showResults()  {



            for(Car car: cars) {

                System.out.println(car.toString());
            }

            }

    public static void filterMileage(int mileage) {
        int count = 0;
        for (Car car : cars) {
            if (car.getMileage() < mileage) {
                count++;
                System.out.println(car.toString());
            }
        }
        if (count == 0) {
            System.out.println("Ничего не найдено :(");
        }

    }

    public static void filterExist(boolean exist) {
        int count = 0;
        for (Car car : cars)
            if (car.isExist() == exist) {
                count++;
                System.out.println(car.toString());
            }
        if (count == 0) {
            System.out.println("Ничего не найдено :(");
        }

    }

    public static void filterCondition(boolean damaged) {
        int count = 0;
        for (Car car : cars) {
            if (car.isDamaged()==damaged) {
                count++;
                System.out.println(car.toString());
                if (count == 0) {
                    System.out.println("Ничего не найдено :(");
                }

            }

        }

    }


    public static void createFile (File file){
        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("Не удалось создать файл");
        }


    }

    public static void fillList (){


        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;

            while ((line = reader.readLine()) != null) {
                String[] strings = line.split("\\|");
                int id = Integer.parseInt(strings[0]);
                String brand = strings[1];
                String model = strings[2];
                String color = strings[3];
                int enginePower = Integer.parseInt(strings[4]);
                double mileage = Double.parseDouble(strings[5]);
                boolean exist = Boolean.parseBoolean(strings[6]);
                boolean damaged = Boolean.parseBoolean(strings[7]);
                        cars.add(new Car(id, brand, model, color, enginePower, mileage, exist, damaged));
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

    public static void addCarToList(String car){
        String[] newCar = car.split("\\|");
        try {
            Car createCar = new Car(getLineCountByReader(file)+1, newCar[0], newCar[1], newCar[2], Integer.parseInt(newCar[3]), Integer.parseInt(newCar[4]), Boolean.parseBoolean(newCar[5]), Boolean.parseBoolean(newCar[6]));
            cars.add(createCar);
        }catch (IOException e){
            System.out.println("Невозможно добавить машину");
        }

    }
            }








