public class Car {
    private int id;
    private String model;
    private String brand;
    private String color;
    private int enginePower;
    private double mileage;
    private boolean exist;
    private boolean damaged;

    public int getId() {
        return id;
    }



    public String getModel() {
        return model;
    }



    public String getBrand() {
        return brand;
    }



    public String getColor() {
        return color;
    }



    public int getEnginePower() {
        return enginePower;
    }



    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public boolean isExist() {

        return exist;
    }



    public boolean isDamaged() {
        return damaged;
    }



    public Car(int id, String model, String brand, String color, int enginePower, double mileage, boolean exist, boolean damaged) {
        this.id = id;
        this.model = model;
        this.brand = brand;
        this.color = color;
        this.enginePower = enginePower;
        this.mileage = mileage;
        this.exist = exist;
        this.damaged = damaged;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", enginePower=" + enginePower +
                ", mileage=" + mileage +
                ", exist=" + exist +
                ", damaged=" + damaged +
                '}';
    }
}

