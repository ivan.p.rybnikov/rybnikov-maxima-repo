import java.util.HashMap;
import java.util.Scanner;
//import java.util.Map;

public class Main {

    public static void main(String[] args) {
        HashMap<Integer,Car> boxes = createBoxes(6);
        Scanner scanner = new Scanner(System.in);
        int filterCase = 0;
        do {
        System.out.println("Что хотите сделать с машиной?");
        System.out.println("Введите:" + " 1-Загнать в первый свободный бокс" + ";" + "2-Загнать в указаный бокс, если там свободно" + ";"+" 3-помыть первую в очереди машину" +";"+ " 4-помыть машину в указанном боксе" +";"+ " 5-выгнать машину из бокса"+";"+ " 6-ничего");
        filterCase = scanner.nextInt();
        scanner.nextLine();

            switch (filterCase) {
                case (1):
                    System.out.println("Введите номер машины:");
                    String carNumber1 = scanner.nextLine();
                    Car car1 = new Car(carNumber1, false);
                    insertCar(car1, boxes);
                    System.out.println(boxes);
                    break;


                case (2):
                    System.out.println("Введите номер машины:");
                    String carNumber2 = scanner.nextLine();
                    Car car2 = new Car(carNumber2, false);
                    System.out.println("Введите номер бокса:");
                    int number2 = scanner.nextInt();
                    insertCarToBox(car2, number2, boxes);
                    System.out.println(boxes);
                    break;

                case (3):
                    try {
                        cleanCar(boxes);
                    }catch (NullPointerException e){
                       // System.out.println("Нечего мыть");
                    }
                    for(Car i:boxes.values()){
                        try {
                            System.out.println("Статус помывки: " + i.clean);
                        }catch (NullPointerException e){
                            System.out.println("В боксе пусто");
                        }
                    }
                    break;

                case (4):
                    System.out.println("Введите номер бокса");
                    int number4 = scanner.nextInt();
                    try {
                        cleanCarInBox(number4, boxes);
                    }catch (NullPointerException e){
                        System.out.println("Нечего мыть");
                    }

                    System.out.println(boxes);
                    for(Car i:boxes.values()) {
                        try {
                            System.out.println("Статус помывки: " + i.clean);
                        } catch (NullPointerException e) {
                            System.out.println("В боксе пусто");
                        }
                    }
                    break;

                case (5):
                    System.out.println("Введите номер бокса");
                    int number5 = scanner.nextInt();
                    removeCar(number5, boxes);
                    System.out.println(boxes);
                    break;
            }
        }while(filterCase!=6) ;

        System.out.println(boxes);


    }
    static void insertCar(Car car,HashMap<Integer,Car> map){ // Метод помещает машину в первый с начала вободный бокс
        for (int i= 1;i<= map.size();i++){
            if (map.get(i)==null){
                map.put(i,car);
                break;
            }

                }
            }
    static void insertCarToBox(Car car,int number,HashMap<Integer,Car> map) { // метод помещает машину в указанный бокс, если там пусто
        map.putIfAbsent(number, car);
        }


    static void cleanCar(HashMap<Integer,Car> map){ // метод моет машину в первом с конца боксе, если она там есть и она грязная
        for (int i= 1;i<= map.size();i++)
            if (map.get(i)!=null && !map.get(i).clean) {
                map.get(i).clean = true;
                break;
            }
    }
    static void cleanCarInBox(int number,HashMap<Integer,Car> map){ // метод моет машину в указанном боксе, если она там есть и она грязная
        if (!map.get(number).clean) {
            map.get(number).clean = true;
            }
        }
    static void removeCar(int number,HashMap<Integer,Car> map){ // метод удаляет машину из указанного бокса
        map.put(number,null);
    }


    static HashMap<Integer,Car> createBoxes(int number){
        HashMap<Integer,Car> boxMap = new HashMap<>(number);
        for (int i= 1;i<=6;i++){
            boxMap.put(i,null);
        }
        return boxMap;
    }
}
