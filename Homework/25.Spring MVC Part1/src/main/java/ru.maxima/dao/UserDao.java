package ru.maxima.dao;

import ru.maxima.model.User;

import java.util.List;
import java.util.Optional;

public interface UserDao extends CrudDao<User>{
    Optional<User> find(int id);
}
