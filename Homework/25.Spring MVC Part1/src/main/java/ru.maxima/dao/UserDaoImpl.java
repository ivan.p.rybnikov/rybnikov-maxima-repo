package ru.maxima.dao;

import ru.maxima.mapper.UserRowMapper;
import ru.maxima.model.Car;
import ru.maxima.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.*;

@Component
public class UserDaoImpl implements UserDao {

    // language=SQL
    private final String SQL_FIND_ALL = "SELECT users.*, cars.id AS car_id, cars.model AS model, wheels.id AS wheel_id, wheels.type_wheel AS type_wheel, wheels.place AS place, wheels.brand AS brand FROM users LEFT JOIN cars ON users.id = cars.owner_id  LEFT JOIN wheels ON cars.id = wheels.car_id;";
    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    Map<Integer, User> userMap = new HashMap<>();


//    private RowMapper<User> userRowMapper = (ResultSet resultSet, int i) -> {
//        int id = resultSet.getInt("id");
//
//        if (!userMap.containsKey(id)) { // есть такой пол-тель или нет
//            String firstName = resultSet.getString("firstname");
//            String lastName = resultSet.getString("lastname");
//            User user = new User(id, firstName, lastName, new ArrayList<>());
//            userMap.put(id, user);
//        }
//
//        Car car = new Car(
//                resultSet.getInt("car_id"),
//                resultSet.getString("model"),
//                userMap.get(id));
//
//        User userForCar = userMap.get(id);
//        List<Car> carList = userForCar.getCars();
//        carList.add(car);
//
//        return userMap.get(id);
//    };

    @Autowired
    public UserDaoImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(User model) {
    }

    @Override
    public Optional<User> find(int id) {
        return Optional.empty();
    }

    @Override
    public void update(User model) {

    }

    @Override
    public void delete(User model) {

    }

    @Override
    public List<User> findAll() {
        List<User> users = template.query(SQL_FIND_ALL, new UserRowMapper());
        UserRowMapper.userMap.clear();
        return users;
    }

}
