package ru.maxima.mapper;

import ru.maxima.model.Car;
import ru.maxima.model.User;
import org.springframework.jdbc.core.RowMapper;
import ru.maxima.model.Wheel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRowMapper implements RowMapper<User> {
    public static Map<Integer, User> userMap = new HashMap<>();
    Map<Integer, Car> carMap = new HashMap<>();

    @Override
    public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        int id = resultSet.getInt("id");
        int carid = resultSet.getInt("car_id");

        if (!userMap.containsKey(id)) { // есть такой пол-тель или нет
            String firstName = resultSet.getString("firstname");
            String lastName = resultSet.getString("lastname");
            User user = new User(id, firstName, lastName, new ArrayList<>());
            userMap.put(id, user);
        }
        if (!carMap.containsKey(carid)) { // есть такая машина
        Car car = new Car(
                resultSet.getInt("car_id"),
                resultSet.getString("model"),
                userMap.get(id),
                new ArrayList<>());

            carMap.put(carid, car);
        }
        Wheel wheel = new Wheel(
                resultSet.getInt("wheel_id"),
                resultSet.getString("type_wheel"),
                carMap.get(carid),
                resultSet.getString("place"),
                resultSet.getString("brand")
        );
        Car carForWheel = carMap.get(carid);
        List<Wheel> wheelsList = carForWheel.getWheels();
        wheelsList.add(wheel);

        User userForCar = userMap.get(id);
        List<Car> carList = userForCar.getCars();
        carList.add(carMap.get(carid));

        return userMap.get(id);
    }
}
