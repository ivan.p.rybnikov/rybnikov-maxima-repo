package ru.maxima.model;
import lombok.*;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "car")
public class Wheel{
    private int id;
    private String type_wheel;
    private Car car;
    private String place;
    private String brand;

}
