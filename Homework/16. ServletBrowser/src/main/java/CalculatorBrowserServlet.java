import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/calculatorbrowser")
public class CalculatorBrowserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String param1 = req.getParameter("first");
        String param2 = req.getParameter("second");
        int i1 = Integer.parseInt(param1);
        int i2 = Integer.parseInt(param2);
        int i3 = i1*i2;
        PrintWriter writer = resp.getWriter();
        writer.write("<h1>Operation result</h1>");
        writer.write(String.valueOf(i3));
    }
}


