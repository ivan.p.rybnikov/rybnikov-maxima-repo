import java.util.ArrayList;

import static java.lang.Math.*;

public interface Motion {
    void move();

    default double length(double[] a, double[] b) {    //Расстояние между точками, метод общий для всех наследников

        return sqrt(pow((a[0] - b[0]), 2) + pow((a[1] - b[1]), 2));
    }

    default double[][] maxPoints(double[][] points) {  // Ищет две самые удалённые точки, метод общий для всех наследников
        double maxLength = 0;
        double[][] maxPoints = new double[2][2];
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points.length; j++) {
                if (maxLength < length(points[i], points[j])) {
                    maxLength = length(points[i], points[j]);
                    maxPoints[0] = points[i];
                    maxPoints[1] = points[j];
                }
            }
        }
        return maxPoints;
    }

    default double distanceToSide(double[][] maxPoints, double[] point) {  //расстояние от точки до прямой, метод общий для всех наследников

        return abs((maxPoints[0][1] - maxPoints[1][1]) * point[0] - (maxPoints[0][0] - maxPoints[1][0]) * point[1] + (maxPoints[0][0] * maxPoints[1][1] - maxPoints[1][0] * maxPoints[0][1])) / (sqrt(pow((maxPoints[0][1] - maxPoints[1][1]), 2) + pow((maxPoints[0][0] - maxPoints[1][0]), 2)));
    }

    default double projectionToLine(double[][] maxPoints, double[] point) {  // длина проекции на прямую, метод общий для всех наследников
        return ((maxPoints[1][0] - maxPoints[0][0]) * (point[0] - maxPoints[0][0]) + (maxPoints[1][1] - maxPoints[0][1]) * (point[1] - maxPoints[0][1])) / sqrt(pow((maxPoints[1][0] - maxPoints[0][0]), 2) + pow((maxPoints[1][1] - maxPoints[0][1]), 2));
    }

    default double[][] notMaxPoints(double[][] points) {  // массив из точек, кроме самых удалённых, метод общий для всех наследников
        ArrayList<double[]> notMaxPoints = new ArrayList<>();
        for (int i = 0; i < points.length; i++) {
            notMaxPoints.add(points[i]);
        }

        for (int i = 0; i < notMaxPoints.size(); i++) {
            for (int j = 0; j < 2; j++) {
                if (notMaxPoints.get(i) == maxPoints(points)[j]) {
                    notMaxPoints.remove(i);
                }

            }

        }
        return notMaxPoints.toArray(new double[notMaxPoints.size()][2]);
    }

}
