import static java.lang.Math.*;

public class Round extends Ellipse implements Square, Perimeter{
    public Round(double[] a, double[] b, double[] c, double[] d) {
        super(a, b, c, d);
    }
    @Override
    public void move(){
        a = new double[]{length(maxPoints(pointsArray)[0],maxPoints(pointsArray)[1])/2,0};
        b = new double[]{-a[0],0};
        c = new double[]{0,a[0]};      //двигаем круг в центр с нулём
        d = new double[]{0,-a[0]};
    }

    @Override
    public void square(){  //вычисляем площадь по формуле для круга
        System.out.println("Площадь круга = " + PI*pow(a[0],2));
    }
    @Override    //вычисляем периметр по формуле для круга
    public void perimeter(){
        System.out.println("Длина окружности = " + 2*PI*a[0]);
    }
}
