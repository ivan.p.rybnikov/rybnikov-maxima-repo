public abstract class Oval implements Motion{
    double[] a;
    double[] b;             //координаты вершин овала
    double[] c;
    double[] d;
    double[][] pointsArray;

    public Oval(double[] a, double[] b, double[] c, double[] d) {

        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.pointsArray = new double[][]{a,b,c,d};
    }

    @Override
    public  void move(){      // двигаем овал в центр с нулём
        double[][] maxPoints = maxPoints(pointsArray);
        a = new double[]{length(maxPoints[0],maxPoints[1])/2,0};
        b = new double[]{-a[0],0};
        c = new double[]{0,length(notMaxPoints(pointsArray)[0],notMaxPoints(pointsArray)[1])/2};
        d = new double[]{0,-c[1]};

    }
}
