import static java.lang.Math.*;

public class Ellipse extends Oval implements  Square, Perimeter{
    public Ellipse(double[] a, double[] b, double[] c, double[] d) {
        super(a, b, c, d);
    }


    @Override
    public  void square(){  //вычисляем полщадь по формулам эллипса
        System.out.println("Площадь эллипса = "+ PI*a[0]*c[1]);
    }
    @Override
    public  void perimeter(){  //вычисляем периметр по формулам эллипса
        System.out.println("Периметр эллипса = "+ PI*(a[0]+c[1]));
    }
}
