import java.util.ArrayList;

import static java.lang.Math.abs;

public class Romb extends Parallelogramm{

    public Romb(double[] a, double[] b, double[] c, double[] d) {
        super(a, b, c, d);
    }
    @Override
    public void move (){
        double[][] maxPoints = maxPoints(pointsArray); // по аналогии с параллелограммом, пользуясь геометрическими свойствами
        a = new double[]{0, 0};
        b = new double[]{length(maxPoints[0],maxPoints[1]),0};
        c = new double[]{projectionToLine(maxPoints,notMaxPoints(pointsArray)[0]),distanceToSide(maxPoints,notMaxPoints(pointsArray)[0])};
        d = new double[]{c[0],-c[1]};


    }
    @Override
    public void square(){
        System.out.println("Площадь ромба = " + abs(b[0]*c[1]));

    }

    @Override
    public void perimeter(){
        System.out.println("Периметр ромба = " + (4*length(a,c)));
    }
}
