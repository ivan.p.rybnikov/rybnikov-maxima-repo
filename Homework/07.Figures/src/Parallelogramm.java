
import static java.lang.Math.*;

public class Parallelogramm implements Square,Perimeter,Motion {
    double[] a;
    double[] b;     //координаты вершин параллелограмма
    double[] c;
    double [] d;
    double[][]pointsArray;

    public Parallelogramm(double[] a, double[] b, double[] c, double[] d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.pointsArray = new double[][]{a, b, c,d};
    }

    @Override
    public void move() {
        double[][] maxPoints = maxPoints(pointsArray);
        a = new double[]{0, 0}; //помещаем вершину в ноль
        b = new double[]{length(maxPoints[0],maxPoints[1]),0}; // по оси x откладываем наибольшую диагональ
        // третья точка по аналогии с треугольником
        c = new double[]{projectionToLine(maxPoints,notMaxPoints(pointsArray)[0]),distanceToSide(maxPoints,notMaxPoints(pointsArray)[0])};
        // 4 точка симметрична третьей относительно оси х
        d = new double[]{projectionToLine(maxPoints,notMaxPoints(pointsArray)[1]),-c[1]};
    }

    @Override
    public void square(){   // вычисляем площадь передвинутой фигуры
        System.out.println("Площадь параллелограмма = " + abs(b[0]*c[1]));

    }

    @Override               //вычисляем периметр передвинутой фигуры
    public void perimeter(){
        System.out.println("Периметр параллелограмма = " + (2*length(a,c)+2*length(a,d)));
    }


}
