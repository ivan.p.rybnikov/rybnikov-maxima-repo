import static java.lang.Math.abs;

public class Rectangle extends Parallelogramm{
    public Rectangle(double[] a, double[] b, double[] c, double[] d) {
        super(a, b, c, d);
    }
    @Override
    public void square(){
        System.out.println("Площадь прямоугольника = " + abs(b[0]*c[1]));

    }

    @Override
    public void perimeter(){
        System.out.println("Периметр прямоугольника = " + (2*length(a,c)+2*length(a,d)));
    }
}

