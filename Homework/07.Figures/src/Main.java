import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Triangle triangle = new Triangle(new double[]{1,2},new double[]{3,4},new double[]{2,-2});
        Parallelogramm parallelogramm = new Parallelogramm(new double[]{0,0},new double[]{0,1},new double[]{1,1},new double[]{1,0});
        Romb romb = new Romb(new double[]{-1,0},new double[]{1,0},new double[]{0,2},new double[]{0,-2});
        Rectangle rectangle = new Rectangle(new double[]{-2,1},new double[]{2,1},new double[]{-2,-1},new double[]{2,-1});
        Kvadrat kvadrat = new Kvadrat(new double[]{1,1},new double[]{-1,1},new double[]{-1,-1},new double[]{1,-1});
        Ellipse ellipse = new Ellipse(new double[]{0,2},new double[]{-8,2},new double[]{-4,5},new double[]{-4,-1});
        Round round = new Round(new double[]{0,2},new double[]{-8,2},new double[]{-4,6},new double[]{-4,-2});
        triangle.move();
        romb.move();
        rectangle.move();
        kvadrat.move();
        ellipse.move();
        round.move();
        parallelogramm.move();



        triangle.perimeter();
        triangle.square();



        parallelogramm.square();
        parallelogramm.perimeter();

        romb.square();
        romb.perimeter();

        rectangle.square();
        rectangle.perimeter();

        kvadrat.square();
        kvadrat.perimeter();

        ellipse.square();
        ellipse.perimeter();

        round.square();
        round.perimeter();






    }
}
