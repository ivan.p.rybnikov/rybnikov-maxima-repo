public class Triangle implements Motion,Square,Perimeter{
    double[] a ;
    double[] b ;  //координаты вершин треугольника
    double[] c ;
    double[][]pointsArray;
    public Triangle(double[] a, double[] b, double[] c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.pointsArray = new double[][]{a, b, c};
    }

    @Override
    public void move() {
        double[][] maxPoints = maxPoints(pointsArray);
        a = new double[]{0, 0};  //двигаем точку в ноль
        b = new double[]{length(maxPoints[0],maxPoints[1]),0}; //максимальнуюсторону откладываем по оси X от нуля
        // третья точка - координата x - длина проекции на мах прямую, координата у - расстояние от этой точки до прямой
        c = new double[]{projectionToLine(maxPoints,notMaxPoints(pointsArray)[0]),distanceToSide(maxPoints,notMaxPoints(pointsArray)[0])};

    }
    @Override
    public void square(){   // вычисляем площадь передвинутой фигуры
        System.out.println("Площадь треугольника = " + b[0]*c[1]/2);

    }

    @Override               //вычисляем периметр передвинутой фигуры
    public void perimeter(){
        System.out.println("Периметр треугольника = " + (length(a,b)+length(a,c)+length(c,b)));
    }

}


