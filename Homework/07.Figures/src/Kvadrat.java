import static java.lang.Math.*;

public class Kvadrat extends Rectangle{
    public Kvadrat(double[] a, double[] b, double[] c, double[] d) {
        super(a, b, c, d);
    }

    @Override
    public void move (){  // по аналогии с параллелограммом, пользуясь геометрическими свойствами
        double[][] maxPoints = maxPoints(pointsArray);
        a = new double[]{0, 0};
        b = new double[]{length(maxPoints[0],maxPoints[1]),0};
        c = new double[]{b[0]/2,b[0]/2};
        d = new double[]{b[0]/2,-b[0]/2};


    }
    @Override
    public void square(){
        System.out.println("Площадь квадрата = " + 2*pow(c[0],2));

    }

    @Override
    public void perimeter(){
        System.out.println("Периметр квадрата = " + (4*sqrt(2*pow(c[0],2))));
    }
}



