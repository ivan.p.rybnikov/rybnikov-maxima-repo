package ru.maxima.springreactdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.maxima.springreactdemo.model.Employee;
import ru.maxima.springreactdemo.repository.EmployeeRepository;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")                     // указываем версию API приложения
@CrossOrigin(origins = {"http://localhost:3000","http://localhost:3000/search-employee"})


// разрешаем React приложению обращаться к SpringBoot приложению
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/employees")   // GET метод, который будет возвращать список работников в React приложение
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @PostMapping("/employees")  // POST метод, который добавляет работника
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping("/employee")   // GET метод, который будет возвращать список работников в React приложение
    public Employee getEmployee(@RequestParam Integer id) {
        return employeeRepository.getReferenceById(id);
    }
}

