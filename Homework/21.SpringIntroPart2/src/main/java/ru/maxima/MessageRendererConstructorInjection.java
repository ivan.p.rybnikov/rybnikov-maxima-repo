package ru.maxima;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageRendererConstructorInjection {
    private MessageRenderer messageRenderer;

    @Autowired
    public MessageRendererConstructorInjection(MessageRenderer messageRenderer) {
        this.messageRenderer = messageRenderer;
    }

    void print(){
        messageRenderer.printMessage();
    }
}
