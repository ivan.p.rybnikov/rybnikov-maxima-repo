package ru.maxima;

import org.springframework.stereotype.Component;

@Component
public class AdvancedMessageRenderer {
    private MessageRenderer messageRenderer;
    public AdvancedMessageRenderer(MessageRenderer messageRenderer) {
        this.messageRenderer = messageRenderer;
    }

    void print(){
        messageRenderer.printMessage();
    }
}
