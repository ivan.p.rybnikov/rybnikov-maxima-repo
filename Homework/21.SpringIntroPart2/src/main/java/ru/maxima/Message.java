package ru.maxima;

public interface Message {
    String getText();
}
