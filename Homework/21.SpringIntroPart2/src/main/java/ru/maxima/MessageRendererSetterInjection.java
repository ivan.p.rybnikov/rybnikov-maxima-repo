package ru.maxima;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageRendererSetterInjection {

    private MessageRenderer messageRenderer;


    @Autowired
    public void setMessageRenderer(MessageRenderer messageRenderer) {

        this.messageRenderer = messageRenderer;
    }

    void print() {messageRenderer.printMessage();}

}
