package ru.maxima;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        MessageRendererSetterInjection renderer = context.getBean(MessageRendererSetterInjection.class);
        renderer.print();

        MessageRendererFieldInjection renderer1 = context.getBean(MessageRendererFieldInjection.class);
        renderer1.print();

        MessageRendererConstructorInjection renderer2 = context.getBean(MessageRendererConstructorInjection.class);
        renderer2.print();
    }
    
}
