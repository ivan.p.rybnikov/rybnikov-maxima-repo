package ru.maxima;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageRendererFieldInjection {
    @Autowired
    private MessageRenderer messageRenderer;

    void print() {messageRenderer.printMessage();}
}
