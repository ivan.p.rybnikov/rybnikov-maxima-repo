package ru.maxima;

public class AlertMessage implements Message{
    private String name;
    private int age;

    private String text;

    public AlertMessage(String name,int age) {
        this.text = name + " you are " + age + " years old!";
    }

    @Override
    public String getText() {
        return text;
    }

}
