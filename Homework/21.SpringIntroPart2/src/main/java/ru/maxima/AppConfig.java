package ru.maxima;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.maxima")
public class AppConfig {

    @Bean
    public Message message(){
        return new HelloMessage("Ivan");
    }

    @Bean
    public MessageRenderer renderer(){
        return new StandardMessageRenderer(message());
    }
}

