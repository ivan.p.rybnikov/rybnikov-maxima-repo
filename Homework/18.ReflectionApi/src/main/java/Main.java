import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) throws Exception {
        SomeClass someObject = new SomeClass();
        Class<? extends SomeClass> aClass = someObject.getClass();
        Field intField = aClass.getField("intField");

        Field privateFiled = aClass.getDeclaredField("privateFiled"); // вводим переменную, соответствующему приватному полю
        System.out.println(someObject.getPrivateFiled());
        privateFiled.setAccessible(true); // даем доступ к приватному полю
        privateFiled.set(someObject, "privateFiled");// меняем приватное поле
        System.out.println(someObject.getPrivateFiled());
        Constructor<?>[] constructors = aClass.getConstructors();
        for (Constructor constructor : constructors) { //Выводим все конструкторы и их аргументы
            Class[] paramTypes = constructor.getParameterTypes();
            for (Class paramType : paramTypes) {
                System.out.print(paramType.getName() + " ");
            }
            System.out.println();
        }
        //Создаем новый объект рефлексией и передаем аргументы, в том числе для приватного поля
        someObject = aClass.getConstructor(new Class[]{String.class, String.class}).newInstance("Newpublic","NewPrivate");

        System.out.println(someObject.toString());
    }
}
