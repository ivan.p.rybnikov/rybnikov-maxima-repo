public class SomeClass {
    public int intField;
    public boolean booleanField;
    public String publicFiled;

    private String privateFiled;

    public SomeClass(int intField, boolean booleanField, String publicFiled) {
        this.intField = intField;
        this.booleanField = booleanField;
        this.publicFiled = publicFiled;
    }

    public SomeClass(String publicFiled,String privateFiled) {
        this.privateFiled = privateFiled;
        this.publicFiled = publicFiled;
    }
    public SomeClass(){

    };

    public String getPrivateFiled() {
        return privateFiled;
    }
    public String toString() {
        return this.intField + " " + this.booleanField + " " + this.privateFiled + " " + this.publicFiled;
    }
}
