package com.example.springrestdemo.controller;

import com.example.springrestdemo.exception.UserNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("users")
public class UserController {

    private int count = 4;

    List<Map<String, String>> users = new ArrayList<>() {{
        add(new HashMap<>() {{
            put("id", "1");
            put("name", "First Name");
        }});
        add(new HashMap<>() {{
            put("id", "2");
            put("name", "Second Name");
        }});
        add(new HashMap<>() {{
            put("id", "3");
            put("name", "Third Name");
        }});
    }};

    private Map<String, String> getMapUser(String id) {
        return users.stream()
                .filter(user -> user.get("id").equals(id))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @GetMapping
    public List<Map<String, String>> getUsers() {
        return users;
    }

    @GetMapping("{id}")
    public Map<String, String> getUser(@PathVariable("id") String id) {
        return getMapUser(id);
    }


    @PostMapping
    public Map<String, String> create(@RequestBody Map<String, String> user) {
        user.put("id", String.valueOf(count++));
        users.add(user);
        return user;
    }

    @PutMapping("{id}")
    public Map<String, String> update(@PathVariable("id") String id, @RequestBody Map<String, String> updatedUser) {
        Map<String, String> initialUser = getMapUser(id);
        initialUser.putAll(updatedUser);
        initialUser.put("id", String.valueOf(id));
        return initialUser;
    }

    @DeleteMapping({"{id}"})
    public void deleteUser(@PathVariable("id") String id) {
        Map<String, String> deletedUser = this.getMapUser(id);
        this.users.remove(deletedUser);
    }
}
