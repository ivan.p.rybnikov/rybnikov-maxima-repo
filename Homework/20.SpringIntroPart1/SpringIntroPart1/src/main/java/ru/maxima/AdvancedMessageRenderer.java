package ru.maxima;

public class AdvancedMessageRenderer {
    private MessageRenderer messageRenderer;
    public AdvancedMessageRenderer(MessageRenderer messageRenderer) {
        this.messageRenderer = messageRenderer;
    }

    void print(){
        messageRenderer.printMessage();
    }
}
