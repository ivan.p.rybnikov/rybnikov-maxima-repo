package ru.maxima;

public class MessageRendererPro {
    private AdvancedMessageRenderer advancedMessageRenderer;

    public MessageRendererPro(AdvancedMessageRenderer advancedMessageRenderer){this.advancedMessageRenderer = advancedMessageRenderer;}

    void printAlert() {advancedMessageRenderer.print();}

}
