package ru.maxima;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        AdvancedMessageRenderer renderer = context.getBean(AdvancedMessageRenderer.class);
        renderer.print();
    }
    
}
