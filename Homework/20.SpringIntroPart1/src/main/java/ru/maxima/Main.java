package ru.maxima;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;


public class Main {
    public static void main(String[] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        MessageRendererPro renderer = context.getBean(MessageRendererPro.class);
        try (Writer writer = new PrintWriter(System.out)) {
            renderer.printAlert(writer);
        }
    }
}
