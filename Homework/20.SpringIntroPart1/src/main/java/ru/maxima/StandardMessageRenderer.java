package ru.maxima;

import java.io.IOException;
import java.io.Writer;

public class StandardMessageRenderer implements MessageRenderer {
    private final Message message;

    public StandardMessageRenderer(Message message) {
        this.message = message;
    }

    @Override
    public void printMessage(Writer writer) throws IOException {
        writer.write(message.getText());
//        System.out.println(message.getText());
    }
}
