package ru.maxima;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;

@Component
public class AdvancedMessageRenderer {
    private MessageRenderer messageRenderer;
    public AdvancedMessageRenderer(MessageRenderer messageRenderer) {
        this.messageRenderer = messageRenderer;
    }

    void print(Writer writer) throws IOException {
        messageRenderer.printMessage(writer);
    }
}
