package ru.maxima;

import java.io.IOException;
import java.io.Writer;

public class MessageRendererPro {
    private AdvancedMessageRenderer advancedMessageRenderer;

    public MessageRendererPro(AdvancedMessageRenderer advancedMessageRenderer){this.advancedMessageRenderer = advancedMessageRenderer;}

    void printAlert(Writer writer) throws IOException {advancedMessageRenderer.print(writer);}

}
