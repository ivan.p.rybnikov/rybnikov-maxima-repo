package ru.maxima;

import java.io.IOException;
import java.io.Writer;

public interface MessageRenderer {
        void printMessage(Writer writer) throws IOException;

}
