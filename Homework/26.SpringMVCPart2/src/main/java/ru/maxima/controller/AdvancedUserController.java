package ru.maxima.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.maxima.dao.UserDao;
import ru.maxima.form.UserForm;
import ru.maxima.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Controller
public class AdvancedUserController {

    @Autowired
    private UserDao userDao;

    @GetMapping("/v2/users")
    public ModelAndView getAllUsers(@RequestParam(value = "first_name", required = false) String firstName) {
        final List<User> users;
        if (firstName != null) {
            users = userDao.findByFirstName(firstName);
        } else {
            users = userDao.findAll();
        }
        ModelAndView modelAndView = new ModelAndView("users");
        modelAndView.addObject("usersFromServer", users);
        return modelAndView;
    }

    @GetMapping("/v2/users/{user_id}")
    public ModelAndView getUserById(@PathVariable("user_id") int userId){
        List<User> users = new ArrayList<>();
        userDao.find(userId).ifPresent(users::add);
        ModelAndView modelAndView = new ModelAndView("users");
        modelAndView.addObject("usersFromServer", users);
        return modelAndView;
    }

    @PostMapping("/v2/users")
    public String addUser(UserForm userForm) {
        User user = User.form(userForm);    // добавляем нового пользователя
        userDao.save(user);
        return "redirect:/v2/users";    // перенапрявляем на эту же самую страницу
    }
}
