package ru.maxima.controller;

import ru.maxima.dao.UserDao;
import ru.maxima.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserController implements Controller {

    @Autowired
    private UserDao userDao;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        if (request.getMethod().equals("GET")) {
            List<User> users = userDao.findAll();
            ModelAndView modelAndView = new ModelAndView("users");
            modelAndView.addObject("usersFromServer", users);
            return modelAndView;
        }
        return null;
    }
}
