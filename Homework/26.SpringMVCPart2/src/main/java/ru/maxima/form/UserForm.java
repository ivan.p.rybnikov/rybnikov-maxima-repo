package ru.maxima.form;

import lombok.Data;

@Data
public class UserForm {
    private String firstName;
    private String lastName;
}
