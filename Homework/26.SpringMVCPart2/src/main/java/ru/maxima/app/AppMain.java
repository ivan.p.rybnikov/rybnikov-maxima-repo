package ru.maxima.app;

import ru.maxima.dao.UserDao;
import ru.maxima.dao.UserDaoImpl;
import ru.maxima.model.User;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;

/**
 * HkdnC9T6OJdAoLS1qB26
 */

public class AppMain {
    public static void main(String[] args) {
        // connect to DB
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUsername("postgres");
        dataSource.setPassword("379159");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");

        // используем DAO для работы с БД
        UserDao userDao = new UserDaoImpl(dataSource);
        List<User> users = userDao.findAll();
        System.out.println(users);
    }
}
