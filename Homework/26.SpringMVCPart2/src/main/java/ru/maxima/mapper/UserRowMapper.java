package ru.maxima.mapper;

import ru.maxima.model.Car;
import ru.maxima.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRowMapper implements RowMapper<User> {
    Map<Integer, User> userMap = new HashMap<>();


    @Override
    public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        int id = resultSet.getInt("id");

        if (!userMap.containsKey(id)) { // есть такой пол-тель или нет
            String firstName = resultSet.getString("firstname");
            String lastName = resultSet.getString("lastname");
            User user = new User(id, firstName, lastName, new ArrayList<>());
            userMap.put(id, user);
        }

        Car car = new Car(
                resultSet.getInt("car_id"),
                resultSet.getString("model"),
                userMap.get(id));

        User userForCar = userMap.get(id);
        List<Car> carList = userForCar.getCars();
        carList.add(car);

        return userMap.get(id);
    }
}
