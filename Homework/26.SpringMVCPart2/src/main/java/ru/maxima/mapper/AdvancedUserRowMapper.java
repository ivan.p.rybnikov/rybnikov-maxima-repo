package ru.maxima.mapper;

import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import ru.maxima.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AdvancedUserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        return User.builder()
                .Id(rs.getInt("id"))
                .firstName(rs.getString("firstname"))
                .lastName(rs.getString("lastname"))
                .build();
    }
}
