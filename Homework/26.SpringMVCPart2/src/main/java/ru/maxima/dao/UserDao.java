package ru.maxima.dao;

import ru.maxima.model.User;

import java.util.List;

public interface UserDao extends CrudDao<User>{
    List<User> findByFirstName(String firstName);
}
