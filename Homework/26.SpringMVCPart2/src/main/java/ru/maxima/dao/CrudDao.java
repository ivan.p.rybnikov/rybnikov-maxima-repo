package ru.maxima.dao;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {
    void save(T model);

    Optional<T> find(int id);

    void update(T model);

    void delete(T model);

    List<T> findAll();
}
