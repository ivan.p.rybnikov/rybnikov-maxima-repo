package ru.maxima.dao;

import org.springframework.dao.EmptyResultDataAccessException;
import ru.maxima.mapper.AdvancedUserRowMapper;
import ru.maxima.mapper.UserRowMapper;
import ru.maxima.model.Car;
import ru.maxima.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.*;

@Component
public class UserDaoImpl implements UserDao {

    private final JdbcTemplate template;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    Map<Integer, User> userMap = new HashMap<>();


    private final RowMapper<User> userRowMapper = (ResultSet resultSet, int i) -> {
        int id = resultSet.getInt("id");

        if (!userMap.containsKey(id)) { // есть такой пол-тель или нет
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            User user = new User(id, firstName, lastName, new ArrayList<>());
            userMap.put(id, user);
        }

        Car car = new Car(
                resultSet.getInt("car_id"),
                resultSet.getString("model"),
                userMap.get(id));

        User userForCar = userMap.get(id);
        List<Car> carList = userForCar.getCars();
        carList.add(car);

        return userMap.get(id);
    };

    @Autowired
    public UserDaoImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(User model) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("firstName", model.getFirstName());
        paramMap.put("lastName", model.getLastName());
        // language=SQL
        String SQL_INSERT_USER = "INSERT INTO users(firstname, lastname) VALUES (:firstName, :lastName)";
        namedParameterJdbcTemplate.update(SQL_INSERT_USER, paramMap);
    }

    @Override
    public List<User> findByFirstName(String firstName) {
        return null;
    }

    @Override
    public void update(User model) {

    }

    @Override
    public void delete(User model) {

    }

    @Override
    public List<User> findAll() {
        // language=SQL
        String SQL_FIND_ALL = "SELECT users.*, cars.id AS car_id, cars.model AS model FROM users LEFT JOIN cars ON users.id = cars.owner_id;";
        List<User> users = template.query(SQL_FIND_ALL, new UserRowMapper());
        userMap.clear();
        return users;
    }

    @Override
    public Optional<User> find(int id) {
        // language=SQL
        String SQL_SELECT_BY_ID = "SELECT * FROM users WHERE id = ?";
        Optional <User> user;

        try {user  = Optional.ofNullable(template.queryForObject(SQL_SELECT_BY_ID, new AdvancedUserRowMapper(), id));}
        catch (EmptyResultDataAccessException e){
            user = Optional.empty();
        }
        return user;
    }
}
