package ru.maxima.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "owner")
public class Car {
    private int id;
    private String model;
    private User owner;
}
