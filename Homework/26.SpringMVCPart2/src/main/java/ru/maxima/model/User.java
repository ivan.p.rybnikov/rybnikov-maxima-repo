package ru.maxima.model;

import lombok.*;
import ru.maxima.form.UserForm;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    private int Id;
    private String firstName;
    private String lastName;
    private List<Car> cars;

    public static User form(UserForm userForm){
        return User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .build();
    }
}
