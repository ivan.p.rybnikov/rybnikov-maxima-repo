public class Car {
    int id;
    String model;
    String brand;
    String color;
    int enginePower;
    double mileage;
    boolean exist;
    boolean damaged;

    public Car(int id,String brand,String model, String color, int enginePower,double mileage,boolean exist,boolean damaged){
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.enginePower = enginePower;
        this.mileage = mileage;
        this.exist = exist;
        this.damaged = damaged;


    };
}
