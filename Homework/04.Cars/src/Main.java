import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static ArrayList<Car> cars = new ArrayList<Car>();

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        boolean addCar = true;
        boolean addFilter = true;

        while (addCar) {
            System.out.println("Хотите добавить машину?(true/false)");
            addCar = scanner.nextBoolean();
            if (addCar) {
                addCar(cars);
            } else {
                break;
            }
        }
        System.out.println("Вывести машины на экран?(true/false)");
        boolean choice = scanner.nextBoolean();
        if (choice) {
            showResults();
        }

        while (addFilter) {
            System.out.println("Хотите отфильтровать?(true/false)");
            addFilter = scanner.nextBoolean();
            if (addFilter) {
                System.out.println("Введите:" + " 1-Фильтр по пробегу" + "," + "2-Фильтр по наличию" + " 3-Фильтр по состоянию");
                int filterCase = scanner.nextInt();
                switch (filterCase) {
                    case (1):
                        System.out.println("Введите максимальный пробег:");
                        int mileage = scanner.nextInt();
                        filterMileage(mileage);
                        break;

                    case (2):
                        System.out.println("Машина в наличии?(true/false)");
                        boolean exist = scanner.nextBoolean();
                        filterExist(exist);
                        break;

                    case (3):
                        System.out.println("Машина битая?(true/false)");
                        boolean damaged = scanner.nextBoolean();
                        filterCondition(damaged);
                        break;
                }

            } else {
                break;
            }
        }

    }


    public static void addCar(ArrayList array) {
        Scanner scanner = new Scanner(System.in);
        int id;
        String model;
        String brand;
        String color;
        int enginePower;
        double mileage;
        boolean exist;
        boolean damaged;

        System.out.println("Введите марку машины:");
        brand = scanner.nextLine();
        System.out.println("Введите модель машины:");
        model = scanner.nextLine();
        System.out.println("Введите цвет машины:");
        color = scanner.nextLine();
        System.out.println("Введите мощность:");
        enginePower = scanner.nextInt();
        System.out.println("Введите пробег:");
        mileage = scanner.nextDouble();
        System.out.println("Машина в наличии?(true/false)");
        exist = scanner.nextBoolean();
        System.out.println("Машина повреждена?(true/false)");
        damaged = scanner.nextBoolean();
        array.add(new Car(id = array.size() + 1, brand, model, color, enginePower, mileage, exist, damaged));
    }

    public static void showResults() {
        for (int i = 0; i < cars.size(); i++) {
            System.out.println(cars.get(i).id);
            System.out.println(cars.get(i).brand);
            System.out.println(cars.get(i).model);
            System.out.println(cars.get(i).color);
            System.out.println(cars.get(i).enginePower);
        }


    }

    public static void filterMileage(int mileage) {
        int count = 0;
        for (int i = 0; i < cars.size(); i++) {
            if (cars.get(i).mileage < mileage) {
                count++;
                System.out.println(cars.get(i).id);
                System.out.println(cars.get(i).brand);
                System.out.println(cars.get(i).model);
                System.out.println(cars.get(i).color);
                System.out.println(cars.get(i).enginePower);
            }
            if (count == 0) {
                System.out.println("Ничего не найдено :(");
            }
        }

    }

    public static void filterExist(boolean exist) {
        int count = 0;
        for (int i = 0; i < cars.size(); i++) {
            if (cars.get(i).exist == exist) {
                count++;
                System.out.println(cars.get(i).id);
                System.out.println(cars.get(i).brand);
                System.out.println(cars.get(i).model);
                System.out.println(cars.get(i).color);
                System.out.println(cars.get(i).enginePower);
            }
            if (count == 0) {
                System.out.println("Ничего не найдено :(");
            }
        }
    }
        public static void filterCondition ( boolean damaged){
            int count = 0;
            for (int i = 0; i < cars.size(); i++) {
                if (cars.get(i).damaged == damaged) {
                    count++;

                    System.out.println(cars.get(i).id);
                    System.out.println(cars.get(i).brand);
                    System.out.println(cars.get(i).model);
                    System.out.println(cars.get(i).color);
                    System.out.println(cars.get(i).enginePower);
                }
                if (count == 0) {
                    System.out.println("Ничего не найдено :(");
                }
            }
        }
    }
