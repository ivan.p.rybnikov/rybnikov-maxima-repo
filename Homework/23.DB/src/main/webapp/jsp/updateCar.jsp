<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Please update model
    </div>
    <form method="post" action="/updatecars">
        <label for="model">Model
            <input class="input-field" type="text" id="model" name="model">
        </label>

        <input type="submit" value="Update cars">
    </form>
</div>
</body>
</html>