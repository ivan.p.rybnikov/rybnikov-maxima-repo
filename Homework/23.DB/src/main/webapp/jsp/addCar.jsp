<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <link href="/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
  <div class="form-style-2-heading">
    Please add car
  </div>
  <form method="post" action="/cars">
    <label for="model">Model
      <input class="input-field" type="text" id="model" name="model">
    </label>
    <label for="owner_id">Owner ID
      <input class="input-field" type="text" id="owner_id" name="owner_id">
    </label>
    <input type="submit" value="Add car">
  </form>
</div>
</body>
</html>