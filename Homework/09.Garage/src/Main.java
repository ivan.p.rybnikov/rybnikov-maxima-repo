public class Main {
    public static void main(String[] args) {
        Driver driver = new Driver("Ivan");
        Car[] carsGarage2 = new Car[5];
        Car[] carsGarage1 = new Car[5];
        carsGarage1[0] = new Car("Subaru","Outback",null);
        carsGarage1[1] = new Car("Toyota","Avensis",driver);
        carsGarage1[2] = new Car("Wolkswagen","Tuareg",driver);
        carsGarage1[3] = new Car("Toyota","Corolla",driver);
        carsGarage1[4] = new Car("Nissan","Pathfinder",driver);
        Garage garage1 = new Garage("Ivanova, 26",carsGarage1);
        Garage garage2 =  new Garage("Russkaya, 38",carsGarage2);

        for (Car car : carsGarage1) {
            car.replaceCar(garage1, garage2);
        }

        System.out.println("Машины в 1 гараже:");

        for (Car car : carsGarage1) {
            if (car != null) {
                System.out.println(car.getBrand());
            } else {
                System.out.println((Object) null);
            }
        }
        System.out.println("Машины во 2 гараже:");

        for (int i=0;i < carsGarage2.length;i++){
            try {
                System.out.println(carsGarage2[i].getBrand());
            }catch (NullPointerException e) {
                System.out.println("Водитель "+ carsGarage1[i].getBrand() + " не найден, машину не перегнали" );
            }
        }
    }
}
