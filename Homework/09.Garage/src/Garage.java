public class Garage {
    private String address;
    private Car[] cars;

    public String getAddress() {
        return address;
    }

    public Car[] getCars() {
        return cars;
    }

    public void setCars(int i,Car car) {
        this.cars[i] = car;
    }

    public Garage(String address, Car[] cars) {
        this.address = address;
        this.cars = cars;
    }

    public Garage(String address) {
        this.address = address;
    }
}
