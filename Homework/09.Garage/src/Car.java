public class Car {
    private String brand;
    private String model;
    private Driver driver;

    public Car(String brand, String model, Driver driver) {
        this.brand = brand;
        this.model = model;
        this.driver = driver;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public void replaceCar(Garage garage1, Garage garage2){
        if (driver!= null) for (int i = 0; i < garage1.getCars().length; i++) {
            if (garage1.getCars()[i] == this) {
                garage1.getCars()[i] = null;
                garage2.setCars(i, this);
            }
        }

    }
}
