import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static ArrayList<Car> cars = new ArrayList<>();

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        boolean addCar = true;
        boolean addFilter = true;

        while (addCar) {
            System.out.println("Хотите добавить машину?(true/false)");
            addCar = scanner.nextBoolean();
            if (addCar) {
                addCar(cars);
            } else {
                System.exit(0);
            }
        }
        System.out.println("Вывести машины на экран?(true/false)");
        boolean choice = scanner.nextBoolean();
        if (choice) {
            showResults();
        }

        while (addFilter) {
            System.out.println("Хотите отфильтровать?(true/false)");
            addFilter = scanner.nextBoolean();
            if (addFilter) {
                System.out.println("Введите:" + " 1-Фильтр по пробегу" + "," + "2-Фильтр по наличию" + " 3-Фильтр по состоянию");
                int filterCase = scanner.nextInt();
                switch (filterCase) {
                    case (1):
                        System.out.println("Введите максимальный пробег:");
                        int mileage = scanner.nextInt();
                        filterMileage(mileage);
                        break;

                    case (2):
                        System.out.println("Машина в наличии?(true/false)");
                        boolean exist = scanner.nextBoolean();
                        filterExist(exist);
                        break;

                    case (3):
                        System.out.println("Машина битая?(true/false)");
                        boolean damaged = scanner.nextBoolean();
                        filterCondition(damaged);
                        break;
                }

            } else {
                break;
            }
        }

    }


    public static void addCar(ArrayList array) {
        Scanner scanner = new Scanner(System.in);
        String model;
        String brand;
        String color;
        int enginePower;
        double mileage;
        boolean exist;
        boolean damaged;
        Car car = new Car();
        car.setId(cars.size()+1);
        System.out.println("Введите марку машины:");
        brand = scanner.nextLine();
        car.setBrand(brand);
        System.out.println("Введите модель машины:");
        model = scanner.nextLine();
        car.setModel(model);
        System.out.println("Введите цвет машины:");
        color = scanner.nextLine();
        car.setColor(color);
        System.out.println("Введите мощность:");
        enginePower = scanner.nextInt();
        car.setEnginePower(enginePower);
        System.out.println("Введите пробег:");
        mileage = scanner.nextDouble();
        car.setMileage(mileage);
        System.out.println("Машина в наличии?(true/false)");
        exist = scanner.nextBoolean();
        car.setExist(exist);
        System.out.println("Машина повреждена?(true/false)");
        damaged = scanner.nextBoolean();
        car.setDamaged(damaged);
        array.add(car);

    }

    public static void showResults() {
        for (Car car : cars) {
            System.out.println(car.getId());
            System.out.println(car.getBrand());
            System.out.println(car.getModel());
            System.out.println(car.getColor());
            System.out.println(car.getEnginePower());
            System.out.println("Наличие машины: " + car.isExist());
            System.out.println("Машина находу: " + car.isDamaged());

        }


    }

    public static void filterMileage(int mileage) {
        int count = 0;
        for (Car car : cars) {
            if (car.getMileage() < mileage) {
                count++;
                System.out.println(car.getId());
                System.out.println(car.getBrand());
                System.out.println(car.getModel());
                System.out.println(car.getColor());
                System.out.println(car.getEnginePower());
                System.out.println("Наличие машины: " + car.isExist());
                System.out.println("Машина находу: " + car.isDamaged());

            }
            if (count == 0) {
                System.out.println("Ничего не найдено :(");
            }
        }

    }

    public static void filterExist(boolean exist) {
        int count = 0;
        for (Car car : cars) {
            if (car.isExist() == exist) {
                count++;
                System.out.println(car.getId());
                System.out.println(car.getBrand());
                System.out.println(car.getModel());
                System.out.println(car.getColor());
                System.out.println(car.getEnginePower());
                System.out.println("Наличие машины: " + car.isExist());
                System.out.println("Машина находу: " + car.isDamaged());
            }
        }
        if (count == 0) {
            System.out.println("Ничего не найдено :(");
        }

    }
        public static void filterCondition ( boolean damaged){
            int count = 0;
            for (Car car : cars) {
                if (car.isDamaged() == damaged) {
                    count++;

                    System.out.println(car.getId());
                    System.out.println(car.getBrand());
                    System.out.println(car.getModel());
                    System.out.println(car.getColor());
                    System.out.println(car.getEnginePower());
                    System.out.println("Наличие машины: " + car.isExist());
                    System.out.println("Машина находу: " + car.isDamaged());

                }
            }
            if (count == 0) {
                System.out.println("Ничего не найдено :(");
            }

        }
    }
