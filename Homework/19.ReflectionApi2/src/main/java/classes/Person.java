package classes;

public class Person {
    public int age;
    public String firstName;
    public String secondName;
    public String middleName;

    public Person(int age, String firstName, String secondName, String middleName) {
        this.age = age;
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
    }

    public Person() {
        this.age = 0;
        this.firstName = "DEFAULT_NAME";
    }

    @Override
    public String toString() {
        return "Person{" +
                "Возраст: " + age +
                ", Имя: " + firstName +
                ",Фамилия: " + secondName +
                ",Отчество: " + middleName + '\'' +
                '}';
    }
}
