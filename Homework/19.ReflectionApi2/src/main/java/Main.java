import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        boolean trueClass = false;
        while (!trueClass) {
            System.out.println("Введите имя класса:");
            String className = scanner.next();
            if (className.equals("classes.Person")) {
                Class<?> aClass = Class.forName(className);
                trueClass = true;
                Field[] fields = aClass.getFields();
                Class[] types = new Class[fields.length];
                for (int i = 0; i < types.length; i++) {
                    types[i] = fields[i].getType();
                }
                Constructor<?> constructor = aClass.getDeclaredConstructor(types);

                System.out.println("Введите возраст:");

                while (!scanner.hasNextInt()){
                    System.out.println("Возраст не корректен, введите заново:");
                    scanner.next();
                }
                int intValue = scanner.nextInt();

                System.out.println("Имя:");
                String stringFirstname = scanner.next();
                System.out.println("Фамилия:");
                String stringSecondname = scanner.next();
                System.out.println("Отчество:");
                String stringMiddlename = scanner.next();


                Object[] arguments = {intValue, stringFirstname, stringSecondname,stringMiddlename };
                Object object = constructor.newInstance(arguments);
                System.out.println(object);
            }
            else {
                System.out.println("Введите корректный класс");
            }
        }

    }
}
