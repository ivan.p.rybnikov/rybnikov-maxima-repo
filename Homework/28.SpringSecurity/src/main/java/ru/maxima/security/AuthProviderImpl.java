package ru.maxima.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.maxima.model.User;
import ru.maxima.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Component  // создаём бин authProviderImpl
public class AuthProviderImpl implements AuthenticationProvider {

    @Autowired  // внедряем бин userRepository
    private UserRepository repository;

    @Override   // метод обрабатывает данные на аутентификацию email and passworf from sign_in.ftl
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();    // получаем параметр email
        User user = repository.findByEmail(email);  // пытаемся достать пользователя по email
        if (user == null) {
            // смотрим нашёлся ли пользователь
            throw new UsernameNotFoundException("User not found");  // если пользователя нет, то выбрасываем исключение
        }
        String password = authentication.getCredentials().toString();   // если есть, то береём пароль из формы
        if (!password.equals(user.getPassword())) {                     // сравниваем пароль из формы с паролем из базы
            throw new BadCredentialsException("Bad credentials");       // если не совпадают, то выбрасываем исключение
        }
        List<GrantedAuthority> authorities = new ArrayList<>();         // пользователи, прошедшие аутентификацию
        return new UsernamePasswordAuthenticationToken(email, password, authorities);    // пользователь авторизован
    }

    @Override
    public boolean supports(Class<?> authentication) {                                       // служебный метод
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
