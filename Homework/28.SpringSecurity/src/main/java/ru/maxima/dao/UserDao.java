package ru.maxima.dao;

import ru.maxima.model.User;

import java.util.List;

public interface UserDao {
    List<User> getAll();

    void add(User user);
}
