package ru.maxima.dao.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.dao.UserDao;
import ru.maxima.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component                          // создаем бин jpaUserDao
@Transactional(readOnly = true)     // помечаем методы класса как только на чтение
public class JpaUserDao implements UserDao {

    @PersistenceContext(unitName = "entityManagerFactory")  // контекст в котором находятся сущности,
                                                            // которые управляются с помощью entityManager
    // entityManagerFactory бин определяется в persistence-config.xml и на его основе строится PersistenceContext
    private EntityManager entityManager;

    @Override
//    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<User> getAll() {
        // language=JPQL
        return entityManager.createQuery("select u from User u", User.class).getResultList();
    }

    @Override
    @Transactional  // помечаем метод add на запись
    public void add(User user) {
        // begin transaction
//        getAll();
        entityManager.persist(user);    // обозначает сущность в PersistenceContext как готовую на запись
    }
}
