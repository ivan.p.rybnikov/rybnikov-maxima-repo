package ru.maxima.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.maxima.model.User;
import ru.maxima.service.UserService;

import java.util.List;

@Controller                                                 // создаём бин
public class UserController {                               // контроллер для стандартных операций

    @Autowired                                              // внедряем ранее созданный бин userService
    private UserService service;

    @GetMapping("/users")                                // запрос возвращает страницу по адресу /users
    public String getUsers(Model model){
        List<User> users = service.getAll();                // с помощью service мы возвращаем список пользователей
        model.addAttribute("usersAttr", users); // с помощью Model users в аттрибут usersAttr
        return "/users";                                    // с помощью View мы указываем куда отправить Model
    }                                                       // отправляем ModelandView


}

