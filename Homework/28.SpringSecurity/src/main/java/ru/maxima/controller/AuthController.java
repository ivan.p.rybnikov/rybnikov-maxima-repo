package ru.maxima.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import ru.maxima.security.MyApplicationListener;
import ru.maxima.model.User;
import ru.maxima.service.UserService;

import javax.servlet.ServletContext;
import java.security.Principal;

@Controller                         // создаём бин
public class AuthController {       // создаём отдельный контроллер для аутентификации, авторизации, регистрации

    @Autowired                      // внедряем ранее созданный бин userService
    WebApplicationContext webApplicationContext;

    @Autowired
    MyApplicationListener myApplicationListener;

    @GetMapping("/sign_up")    // возвращает страницу, заполнив которую мы вызовем метод POST
    public String getSignUp(Model model) {
        model.addAttribute("user",new User());
        return "/auth/sign_up";          // View для вызова метода POST
    }

    @PostMapping("/sign_up")                         // handler по регистрации
    public String signUp(@ModelAttribute User user) {   // блягодаря @ModelAttribute мы создаем нового USER
                                                        // с параметрами, которые указали в sign_up.ftl
                                                       // добавляем в БД через service
        return "redirect:/users";                       // повторно вызываем метод GET getUsers() по endpoint ("/users")
            // и возвращает новый список
    }

    @RequestMapping("/login")
    public String login(){

        return "/auth/sign_in";                          // возвращает страничку для логина или аутентификации
    }

    @GetMapping("/welcome")
    public String welcome(Principal principal, Model model) {
        String userName = principal.getName();
        model.addAttribute("userAttr",userName);
        return "/auth/welcome";          // View для вызова метода POST
    }

    @RequestMapping("/authbad")
    public String authbad( Model model) {
        ServletContext servletContext = webApplicationContext.getServletContext();
        String failedUserName = (String) servletContext.getAttribute("failedUserName");
        model.addAttribute("username", failedUserName);

        return "/auth/sign_up";                          // возвращает страничку для логина или аутентификации
    }
}
