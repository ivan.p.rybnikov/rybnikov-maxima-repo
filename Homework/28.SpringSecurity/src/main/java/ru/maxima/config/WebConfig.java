package ru.maxima.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/**
 * Класс аналогичный dispatcher-servlet.xml
 */
@Configuration                                      // поскольку это конфигурационный файл
@EnableWebMvc                                       // соответствует <mvc:annotation-driven/> в dispatcher-servlet.xml
@ComponentScan("ru.maxima")                         // покажем где искать базовые компоненты
public class WebConfig implements WebMvcConfigurer {

    @Bean                                                                   // создаём бин viewResolver
    public FreeMarkerViewResolver viewResolver() {                          // c типом FreeMarkerViewResolver
        FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver(); // создаём объект viewResolver
        viewResolver.setSuffix(".ftl");                                     // добавляем суффикс к viewResolver
        viewResolver.setContentType("text/html;charset=UTF-8");             // добавляем тип контента к viewResolver
        viewResolver.setCache(false);                                       // не кешируем запросы
        return viewResolver;                                                // возвращаем сконфигурированный объект
    }

    @Bean                                                                   // создаём бин freeMarkerConfigurer
    public FreeMarkerConfigurer freeMarkerConfigurer() {                    // c типом FreeMarkerConfigurer
        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();       // создаём объект configurer
        configurer.setTemplateLoaderPath("/WEB-INF/templates");             // указываем путь к темплейтам
        configurer.setDefaultEncoding("UTF-8");                             // ставим конфигурацию по-умолчанию
        configurer.setFreemarkerSettings(new Properties(){{
            this.put("default_encoding", "UTF-8");                          // настраиваем свойства через ключ-значение
        }});
        return configurer;                                                  // возвращаем бин freeMarkerConfigurer
    }

}
