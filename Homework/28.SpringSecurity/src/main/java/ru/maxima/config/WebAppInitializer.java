package ru.maxima.config;

import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Класс аналогичный web.xml
 */
@Order(1)   // указываем, что данный инит запусткается в первую очередь
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {   // загружает основные конфигурации,
                                                    // импортируем бины из других конфигов
        return new Class<?>[]{PersistenceConfig.class, SecurityConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {    // описывает dispatcher servlet
        return new Class<?>[]{WebConfig.class};         // <servlet> tag in web.xml, возвращаем WebConfig
    }

    @Override
    protected String[] getServletMappings() {           // <servlet-mapping> tag in web.xml
        return new String[]{"/"};
    }
}
