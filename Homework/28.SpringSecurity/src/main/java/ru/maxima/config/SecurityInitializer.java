package ru.maxima.config;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Order(2)   // данный Initializer будет инициализироваться после WebAppInitializer
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
