package ru.maxima.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.maxima.security.AuthProviderImpl;

@Configuration      // поскольку это конфигурационный файл
@EnableWebSecurity  // для того, чтобы работал Spring Security
@ComponentScan("ru.maxima.security")    // указываем где мы можем найти authProvider
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired      // внедряем бин authProvider
    private AuthProviderImpl authProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {  // описываем разделение доступа к различным хендлерам
        http.authorizeRequests()
                .antMatchers("sign_up", "/login").anonymous()    // адреса для неавторизованных пользователей
                .antMatchers("/users","/welcome").authenticated()           // адреса для авторизованных пользователей
                .and().csrf().disable()                                     // отключаем временно csrf
                .formLogin()//.usernameParameter("email").passwordParameter("password").failureForwardUrl("/authbad")                                               // указываем форму логина
                .defaultSuccessUrl("/welcome")
                .failureUrl("/authbad")
                .loginPage("/login")                                        // указываем страницу логина
                .loginProcessingUrl("/login/process")                       // адрес для обработки
                .usernameParameter("email")                                 // используем email вместо username
                .and().logout();                                            // добавляем возможность разлогиниться
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {  // конфигурируется AuthenticationProvider
        auth.authenticationProvider(authProvider);                                  // предоставляем authProvider
    }
}
