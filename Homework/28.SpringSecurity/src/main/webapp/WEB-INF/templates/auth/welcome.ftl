<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
</head>
<body>
<#if userAttr?has_content>
<p>Welcome, ${userAttr}!</p>
</#if>
</body>
</html>