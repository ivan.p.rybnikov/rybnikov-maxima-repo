<html>
<#-- страница для логина -->
<head>
    <title>Login</title>
</head>
<body>
<#-- вызываем метод POST по адресу /login/process -->
<form action="/login/process" method="post">
    <div>
        Email: <input name="email" type="email">
    </div>
    <div>
        Password: <input name="password" type="password">
    </div>
    <input type="submit">
    <#if error??>
        <p>Пароль или логин не верны, попробуйте ещё раз</p>
    </#if>
</form>
</body>
</html>
