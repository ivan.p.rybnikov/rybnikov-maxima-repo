<html>
<head>
    <title>Sign Up</title>
</head>
<body>


<form action="/sign_up" method="post" modelAttribute="user">
    <div>
        <label path="firstname">Name</label>
        <input path="firstname"/>
        <errors path="firstname"/>
    </div>
    <div>
        <label path="lastname">Surname</label>
        <input path="lastname"/>
        <errors path="lastname"/>
    </div>
    <div>
        <label path="email">Email</label>
        <input path="email" type="email" <#if username?has_content>placeholder=${username}</#if>/>
        <errors path="email"/>
    </div>
    <div>
        <label path="password">Password</label>
        <input path="password" type="password" />
        <errors path="password"/>
    </div>
    <input type="submit">
</form>
</body>
</html>