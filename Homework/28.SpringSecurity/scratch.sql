drop table users;

create table users(
    id serial primary key,
    name varchar(255) not null ,
    surname varchar(255) not null ,
    email varchar(255) not null ,
    password varchar(255) not null
);

insert into users(name, surname, email, password)
VALUES ('Ilon', 'Mask', 'ilon@mask.com', '789'),
       ('Joe', 'Biden', 'joe@biden.com', '123'),
       ('Nancy', 'Pelosi', 'crazy@woman.com', '456');

select * from users;